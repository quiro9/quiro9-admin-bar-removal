=== Quiro9 Rename Tool ===
Contributors: quiro9
Tags: admin bar, remove, quiro9
Requires at least: 4.0
Tested up to: 4.0
Stable tag: 0.8
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin remove the "admin bar" for users, network or admin page...

== Description ==
this plugin remove the admin bar, user bar and network bar on top of page... in place (within the page to the top righty) view your user name and "exit" text...

This is a modification (with few changes) of: [wp-admin-bar-removal](https://wordpress.org/plugins/wp-admin-bar-removal/) (licence GNU/GPL2)

== Changelog ==
= 0.8 (master)
* Now load user gravatar picture...
* HTML tags correction...
* link to web site open in new tab...

= 0.6
* packet and autor correction...
* add "zoom and mobile menu" (correction bug)...
* Optimize the Code

== Install ==

* download a .zip file:
	* Or download the master .zip, unzip and into to "quiro9-admin-bar-removal-master" folder compress "quiro9-admin-bar-removal" (exclude .md file), this new zip is a install...

* Upload in your wordpress and execute

* The plugin is automatic (without menu) ++ The effects are reversible, onli you need disable this pluggin ++
