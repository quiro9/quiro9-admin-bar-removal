<?php
/*
Plugin Name: Quiro9 Admin Bar Removal
Plugin URI: https://github.com/quiro9/quiro9-admin-bar-removal
Description: This is a modify of <a href='http://slangji.wordpress.com/wp-admin-bar-removal/'>wp-admin-bar-removal</a>... the plugin disable admin bar or toolbar on WordPress 4.0 for all admin and user roles, completely remove of the page. Instead shows "exit user", the name and photo, and website link in the user or admin panel...: the configuration of this plugin is Automatic!
Version: 0.80
Author: Juan Quiroga
Author URI: https://github.com/quiro9
Requires at least: 4.0
Tested up to: 4.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
/*
 * LICENSING (license.txt)
 * Quiro9 Admin Bar Removal is based on -> "WP Admin Bar Removal" by (//slangji.wordpress.com/) (email: <slangjis [at] googlemail [dot] com>)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the [GNU General Public License](//wordpress.org/about/gpl/)
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see [GNU General Public Licenses](//www.gnu.org/licenses/),
 */
 /* @package Quiro9 Admin Bar Removal
  * @subpackage WordPress Plugin
  * @description Disable WordPress Admin Bar or Toolbar and Remove Code
  * @install The configuration of this plugin is Automatic!
  * @branche 2014
  * @since   4.0+
  * @tested  4.0+
  * @version 0.80
  * @status STABLE (trunk) release
  * @development Code in Becoming!
  * @author quiro9
  * @license GPLv3 or later
  */

global $wp_version;

if ( $wp_version < 4.0 or !defined( 'ABSPATH' )){wp_die( __( 'This Plugin Requires WordPress 4.0 or Greater: Activation Stopped!' ) );}
else{

    // here you can modify CSS style
    function wpabr_style(){
        ?>
        <style type="text/css">
            /* RESET  ALL */
            html.body,html.wp-toolbar, html.wp-toolbar #wpadminbar,
            html.admin-bar, html.admin-bar #adminmenu, html.admin-bar #wpadminbar,
            body #adminmenuback, body .wrap, body #wpbody, body .wp-first-item, body #screen-meta-links
            {margin-top:0px;padding-top:0px !important;}
            /* END RESET */

            /* wp-bar admin */
            body #wpadminbar{position:absolute;z-index:10 !important;padding-top:52px;height:33px;margin-left:0px;left:0px;right:0px;min-width:160px;width:100%;}
            table #adminmenuwrap{min-width:160px;width:100%;height:32px;}
            body #adminmenuwrap_lgt a{text-decoration:none !important;}
            body #adminmenuwrap_lgt a:hover{text-decoration:none !important;}
            body #adminmenuwrap_lgt a:focus{text-decoration:none !important;}
            body #adminmenuwrap_lgt a:active{text-decoration:none !important;}
            body #adminmenuwrap_lgt a:visite{text-decoration:none !important;}
            #gravatar img{height:auto;margin-top:2px;margin-bottom:-10px;-webkit-border-radius: 5px;-o-border-radius: 5px;-ms-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;}
            /* end wp-bar admin */

            /* wp-content */
            body #wpbody #wpbody-content{padding-top:35px;}
            body #wpbody .wrap{margin-top:16px;}
            /* end  wp-content */

            /* ZOOM RESPONSE CSS */
            /* menu option popup view */
            @media only screen and (max-width: 796px){
            body #wp-admin-bar-menu-toggle{width:50px;margin-right:4px;}
            body #profile-page{padding-top:25px !important;}
            }
            /* first left menu element adapt */
            body #menu-dashboard, body #adminmenu {padding:0px;margin:0px;border:0px;}
            @media only screen and (max-width: 768px){
                body #adminmenuwrap{padding-top:48px;margin-top:0px;top:0px;border-top:0px;}}
            @media only screen and (min-width: 769px){
                body #adminmenuwrap{padding-top:36px;margin-top:0px;top:0px;border-top:0px;}}
            /* END ZOOM RESPONSE CSS*/
        </style>
        <?php }

    function wpabr_1st(){
        $path = str_replace( WP_PLUGIN_DIR . '/', '', __FILE__ );
        if ( $plugins = get_option( 'active_plugins' ) )
        {   if ( $key = array_search( $path, $plugins ) )
               {  array_splice( $plugins, $key, 1 );
                  array_unshift( $plugins, $path );
                  update_option( 'active_plugins', $plugins );}
        }
    }

    // new bar
    function wpabr_ablh(){
    ?>
        <table id="wpadminbar">
            <td id="adminmenuwrap_lgt" align="right">
            <?php
                echo '<div style="margin-top:-8px;">';
                wp_get_current_user();

                $current_user = wp_get_current_user();

                if ( !( $current_user instanceof WP_User ) )
                    return;

                $email = $current_user->user_email;
                $grv_hash = md5(strtolower(trim($email)));
                echo '<a id="gravatar" href="http://es.gravatar.com/" target="_blank" alt="Change Gravatar"><img src="http://www.gravatar.com/avatar/'.$grv_hash.'?s=25"></img></a> ';
                echo $current_user->display_name . '';
                echo ' |  <a href="' . home_url() . '" target="_blank"> ' . __( get_bloginfo() ) . ' </a> ';

                if ( is_multisite() && is_super_admin() )
                    {
                        if ( !is_network_admin() )
                            {
                                echo ' | <a href="' . network_admin_url() . '">' . __( 'Network Admin' ) . '</a>';
                            }
                        else
                            {
                                echo ' | <a href="' . get_DashBoard_url( get_current_user_id() ) . '">' . __( 'Site Admin' ) . '</a>';
                            }
                    }

                echo ' | <a href="' . wp_logout_url( home_url() ) . '">' . __( 'Log Out' ) . '</a>';
                echo '</div>';
            ?>
            </td>
            <td id="wp-admin-bar-menu-toggle" align="right"><a href="#"><span class="ab-icon"></span></a></td>
            </tr>
        </table>
    <?php
    }
    // here remove
    function wp_admin_bar_init(){add_filter( 'show_admin_bar', '__return_false' );add_filter( 'wp_admin_bar_class', '__return_false' );}
    function wpabr_ruppoabpc(){echo '<style type="text/css">.show-admin-bar{display:none}</style>';}
    function wpabr_remove_wptbr_abtlh(){if ( has_filter( 'in_admin_header' , 'wptbr_abtlh' ) ){remove_filter( 'in_admin_header' , 'wptbr_abtlh' );}}
    function wpabr_remove_wptrcd_atblh(){if ( has_filter( 'in_admin_header' , 'wptrcd_atblh') ){remove_filter('in_admin_header', 'wptrcd_atblh');}}

    function init_quiro9_login()
    {
    // remove
        remove_action( 'init', 'wp_admin_bar_init' );
        remove_filter( 'init', 'wp_admin_bar_init' );
        remove_action( 'wp_head', 'wp_admin_bar' );
        remove_filter( 'wp_head', 'wp_admin_bar' );
        remove_action( 'wp_footer', 'wp_admin_bar' );
        remove_filter( 'wp_footer', 'wp_admin_bar' );
        remove_action( 'admin_head', 'wp_admin_bar' );
        remove_filter( 'admin_head', 'wp_admin_bar' );
        remove_action( 'admin_footer', 'wp_admin_bar' );
        remove_filter( 'admin_footer', 'wp_admin_bar' );
        remove_action( 'wp_head', 'wp_admin_bar_class' );
        remove_filter( 'wp_head', 'wp_admin_bar_class' );
        remove_action( 'wp_footer', 'wp_admin_bar_class' );
        remove_filter( 'wp_footer', 'wp_admin_bar_class' );
        remove_action( 'admin_head', 'wp_admin_bar_class' );
        remove_filter( 'admin_head', 'wp_admin_bar_class' );
        remove_action( 'admin_footer', 'wp_admin_bar_class' );
        remove_filter( 'admin_footer', 'wp_admin_bar_class' );
        remove_action( 'wp_head', 'wp_admin_bar_css' );
        remove_filter( 'wp_head', 'wp_admin_bar_css' );
        remove_action( 'wp_head', 'wp_admin_bar_dev_css' );
        remove_filter( 'wp_head', 'wp_admin_bar_dev_css' );
        remove_action( 'wp_head', 'wp_admin_bar_rtl_css' );
        remove_filter( 'wp_head', 'wp_admin_bar_rtl_css' );
        remove_action( 'wp_head', 'wp_admin_bar_rtl_dev_css' );
        remove_filter( 'wp_head', 'wp_admin_bar_rtl_dev_css' );
        remove_action( 'admin_head', 'wp_admin_bar_css' );
        remove_filter( 'admin_head', 'wp_admin_bar_css' );
        remove_action( 'admin_head', 'wp_admin_bar_dev_css' );
        remove_filter( 'admin_head', 'wp_admin_bar_dev_css' );
        remove_action( 'admin_head', 'wp_admin_bar_rtl_css' );
        remove_filter( 'admin_head', 'wp_admin_bar_rtl_css' );
        remove_action( 'admin_head', 'wp_admin_bar_rtl_dev_css' );
        remove_filter( 'admin_head', 'wp_admin_bar_rtl_dev_css' );
        remove_action( 'wp_footer', 'wp_admin_bar_js' );
        remove_filter( 'wp_footer', 'wp_admin_bar_js' );
        remove_action( 'wp_footer', 'wp_admin_bar_dev_js' );
        remove_filter( 'wp_footer', 'wp_admin_bar_dev_js' );
        remove_action( 'admin_footer', 'wp_admin_bar_js' );
        remove_filter( 'admin_footer', 'wp_admin_bar_js' );
        remove_action( 'admin_footer', 'wp_admin_bar_dev_js' );
        remove_filter( 'admin_footer', 'wp_admin_bar_dev_js' );
        remove_action( 'locale', 'wp_admin_bar_lang' );
        remove_filter( 'locale', 'wp_admin_bar_lang' );
        remove_action( 'wp_head', 'wp_admin_bar_render', 1000 );
        remove_filter( 'wp_head', 'wp_admin_bar_render', 1000 );
        remove_action( 'wp_footer', 'wp_admin_bar_render', 1000 );
        remove_filter( 'wp_footer', 'wp_admin_bar_render', 1000 );
        remove_action( 'admin_head', 'wp_admin_bar_render', 1000 );
        remove_filter( 'admin_head', 'wp_admin_bar_render', 1000 );
        remove_action( 'admin_footer', 'wp_admin_bar_render', 1000 );
        remove_filter( 'admin_footer', 'wp_admin_bar_render', 1000 );
        remove_action( 'wp_ajax_adminbar_render', 'wp_admin_bar_ajax_render', 1000 );
        remove_filter( 'wp_ajax_adminbar_render', 'wp_admin_bar_ajax_render', 1000 );

    // modify
    add_action( 'activated_plugin', 'wpabr_1st' );
    add_action( 'in_admin_header', 'wpabr_ablh' );
    add_filter( 'show_wp_pointer_admin_bar', '__return_false' );
    add_action( 'admin_print_styles-profile.php', 'wpabr_ruppoabpc' );
    add_filter( 'init', 'wp_admin_bar_init', 9 );
    add_action( 'admin_print_styles', 'wpabr_style', 21 );
    }

    // init of plugin
    add_action( 'init', 'init_quiro9_login');
}
?>
