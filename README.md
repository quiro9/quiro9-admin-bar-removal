@[](quiro9-admin-removal-bar
========================

this plugin remove the admin bar, user bar and network bar on top of page... in place (within the page to the top righty) view your user name and "exit" text...

This is a modification (with few changes) of: [wp-admin-bar-removal](https://wordpress.org/plugins/wp-admin-bar-removal/) (licence GNU/GPL2)

Changelog
-----------------
####0.80 (master)
 * Now load user gravatar picture...
 * HTML tags correction...
 * link to web site open in new tab...
  
####0.60
 * packet and autor correction...
 * add "zoom and mobile menu" (correction bug)...
 * Optimize the Code

Install
------------------
* download a .zip file:

	* 	* Or download [the master .zip](https://github.com/quiro9/quiro9-admin-bar-removal/archive/master.zip), unzip and into to "quiro9-admin-bar-removal-master" folder compress "quiro9-admin-bar-removal" (exclude .md file), this new zip is to install...

* upload in your wordpress and execute
 
* the plugin is automatic (without menu)
++ The effects are reversible, onli you need disable this pluggin ++

__________________________
Please, if you modify the code you need try to comment in English.
Here on github your can use English or Spanish in Issues or Requests...